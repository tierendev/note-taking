//
//  MainViewController.swift
//  Note
//
//  Created by Sothearith Sreang on 24/11/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MainViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    private var sortItem: UIBarButtonItem!
    
    var notes: [Note] = []
    var sort: Sort = .Date
    
    var noteListManager: NoteListManager? = nil
    private let disposables = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        initDependencies()
        initUi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadData()
    }
    
    private func initDependencies() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        noteListManager = appDelegate?.noteListManager
    }
    
}

// MARK: UI

extension MainViewController {
    
    private func initUi() {
        initTableView()
        initNavigationBar()
    }
    
    private func initTableView() {
        tableView.tableFooterView = UIView()
    }
    
    private func initNavigationBar() {
        let addItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(navigateToAddNoteScreen))
        sortItem = UIBarButtonItem(image: UIImage(named: "Sort Date"), style: .plain, target: self, action: #selector(self.onSortButtonClicked(_:)))
        self.navigationItem.rightBarButtonItems = [addItem, sortItem]
        self.navigationItem.title = "Notes"
    }
    
    private func setUpNavigationBar(sort: Sort) {
        let imageName = sort == .Date ? "Sort Date" : "Sort Alphabet"
        sortItem.image = UIImage(named: imageName)
    }
    
    private func populate() {
        self.tableView.reloadData()
        
        let numNotes = notes.isEmpty ? "" : "(\(notes.count))"
        self.navigationItem.title = "Notes \(numNotes)"
    }
    
    @objc private func onSortButtonClicked(_ sender: UIBarButtonItem) {
        let sort = self.sort == .Date ? Sort.Title : Sort.Date
        self.setUpNavigationBar(sort: sort)
        self.sortNotes(sort: sort)
    }
}

// MARK: Navigation

extension MainViewController {
    
    @objc private func navigateToAddNoteScreen() {
        let controller = storyboard?.instantiateViewController(withIdentifier: AddEditNoteViewController.IDENTIFIER) as! AddEditNoteViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func navigateToViewNoteScreen(note: Note) {
        let controller = storyboard?.instantiateViewController(withIdentifier: ViewNoteViewController.IDENTIFIER) as! ViewNoteViewController
        controller.note = note
        self.navigationController?.pushViewController(controller, animated: true)
    }
}


// MARK: TableView Delegates

private let CELL_IDENTIFIER = "NoteTableViewCell"

extension MainViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER) as! NoteTableViewCell
        let note = notes[indexPath.row]
        cell.populate(note)
        return cell
    }
}

extension MainViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let note = notes[indexPath.row]
        navigateToViewNoteScreen(note: note)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let note = notes[indexPath.row]
            deleteNote(note: note)
        }
    }
    
}

// MARK: Data

private protocol MainViewModel {
    func loadData()
    func deleteNote(note: Note)
    func sortNotes(sort: Sort)
}

extension MainViewController : MainViewModel {
    
    func loadData() {
        if let noteListManager = self.noteListManager {
            noteListManager.getNotes(sort: sort)
                .observeOn(MainScheduler.instance)
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(
                    onSuccess: { notes in
                        self.notes = notes
                        self.populate()
                    },
                    onError: { error in
                        print("error", error)
                    }
                )
                .disposed(by: disposables)
        }
    }
    
    func deleteNote(note: Note) {
        if let noteListManager = self.noteListManager {
            noteListManager.deleteNote(note: note)
                .observeOn(MainScheduler.instance)
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(
                    onCompleted: {
                        if let index = self.notes.firstIndex(of: note) {
                            self.notes.remove(at: index)
                        }
                        
                        self.populate()
                },
                    onError: { error in
                        print("error", error)
                }
                )
                .disposed(by: disposables)
        }
    }
    
    func sortNotes(sort: Sort) {
        self.sort = sort
        loadData()
    }
    
}
