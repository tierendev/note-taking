//
//  NoteTableViewCell.swift
//  Note
//
//  Created by Sothearith Sreang on 2/12/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import UIKit

class NoteTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var date: UILabel!
    
    override func awakeFromNib() {
        backgroundColor = UIColor.clear
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        backgroundColor = UIColor.clear
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        backgroundColor = UIColor.clear
    }
    
    func populate(_ note: Note) {
        title.text = note.title
        date.text = note.dateInString()
    }
}
