//
//  ViewNoteViewController.swift
//  Note
//
//  Created by Sothearith Sreang on 4/12/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import UIKit
import RxSwift

class ViewNoteViewController: UIViewController {
    
    static let IDENTIFIER = "ViewNoteViewController"
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var descriptionText: UILabel!
    @IBOutlet weak var doodleView: DoodleView!
    
    var note: Note? = nil
    
    var noteListManager: NoteListManager? = nil
    private let disposables = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDependencies()
        initUI()
        populateUI(note)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let id = self.note?.id {
            loadNote(id)
        }
    }
    
    private func initDependencies() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        noteListManager = appDelegate?.noteListManager
    }
    
    @objc private func navigateToEditScreen() {
        let controller = storyboard?.instantiateViewController(withIdentifier: AddEditNoteViewController.IDENTIFIER) as! AddEditNoteViewController
        controller.note = self.note
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}

// MARK: UI

extension ViewNoteViewController {
    
    private func initUI() {
        initNavigationBar()
    }
    
    private func initNavigationBar() {
        let editItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(navigateToEditScreen))
        self.navigationItem.rightBarButtonItem = editItem
    }
    
    private func populateUI(_ note: Note?) {
        if let note = note {
            titleText.text = note.title
            date.text = note.dateInString()
            descriptionText.text = note.text
            if let lines = note.doodle {
                doodleView.lines = lines
            }
        }
    }
}

// MARK: Data

extension ViewNoteViewController {
    
    private func loadNote(_ id: String) {
        if let noteListManager = self.noteListManager {
            noteListManager.getNote(id: id)
                .observeOn(MainScheduler.instance)
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(
                    onSuccess: { note in
                        self.note = note
                        self.populateUI(note)
                    },
                    onError: { error in
                        print("error", error)
                    }
                )
                .disposed(by: disposables)
        }
    }
    
}
