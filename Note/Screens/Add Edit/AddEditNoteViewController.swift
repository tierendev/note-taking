//
//  AddEditNoteViewController.swift
//  Note
//
//  Created by Sothearith Sreang on 24/11/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class AddEditNoteViewController: UIViewController {

    static let IDENTIFIER = "AddEditNoteViewController"
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextView!
    @IBOutlet weak var doodleView: DoodleView!
    @IBOutlet weak var brush: UIButton!
    @IBOutlet weak var eraser: UIButton!
    
    var note: Note? = nil
    
    var noteListManager: NoteListManager? = nil
    private let disposables = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDependencies()
        initUi()
    }
    
    private func initDependencies() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        noteListManager = appDelegate?.noteListManager
    }
    
    // MARK: Listeners

    @objc private func save() {
        let title = titleTextField.text ?? ""
        let text = descriptionTextField.text ?? ""
        let date = self.note?.date ?? Date()
        let doodle = doodleView.lines
        saveNote(title: title, text: text, date: date, doodle: doodle)
    }
    
    // MARK: Navigation
    
    private func navigateBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: Listeners
    
    
    @IBAction func onBrushClicked(_ sender: Any) {
        doodleView.isErasing = false
        setBrushMode()
    }
    
    @IBAction func onEraserClicked(_ sender: Any) {
        doodleView.isErasing = true
        setEraserMode()
    }
    
}

// MARK: UI

extension AddEditNoteViewController {
    
    private func initUi() {
        initTextFields()
        initNavigationBar()
        populate()
    }
    
    private func initTextFields() {
        //        titleTextField.delegate = self
        //        descriptionTextField.delegate = self
    }
    
    private func initNavigationBar() {
        let saveItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(save))
        self.navigationItem.rightBarButtonItem = saveItem
        self.navigationItem.title = ""
    }
    
    private func populate() {
        populateTextFields(self.note)
        populateDoodleView(self.note)
    }
    
    private func populateTextFields(_ note: Note?) {
        if let note = self.note {
            titleTextField.text = note.title
            descriptionTextField.text = note.text
        }
    }
    
    private func populateDoodleView(_ note: Note?) {
        if let lines = self.note?.doodle {
            doodleView.lines = lines
        }
        
        setBrushMode()
    }
    
    private func setBrushMode() {
        brush.setSelected(true)
        eraser.setSelected(false)
    }
    
    private func setEraserMode() {
        brush.setSelected(false)
        eraser.setSelected(true)
    }
}

// MARK: Data

extension AddEditNoteViewController {
    
    private func saveNote(title: String, text: String, date: Date, doodle: [Line]?) {
        if let noteListManager = self.noteListManager {
            var saveNoteStream: Single<Note>
            
            if let note = self.note {
                let updateNote = Note(id: note.id, title: title, text: text, date: date, doodle: doodle)
                saveNoteStream = noteListManager.updateNote(note: updateNote)
            } else {
                let newNote = Note(id: NSUUID().uuidString, title: title, text: text, date: date, doodle: doodle)
                saveNoteStream = noteListManager.addNote(note: newNote)
            }
            
            saveNoteStream
                .observeOn(MainScheduler.instance)
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(
                    onSuccess: { note in
                        self.navigateBack()
                },
                    onError: { error in
                        print("error", error)
                }
                )
                .disposed(by: disposables)
        }
    }
    
}

private extension UIButton {
    func setSelected(_ isSelected: Bool) {
        if isSelected {
            layer.borderColor = UIColor.black.cgColor
            layer.borderWidth = 1.5
            layer.cornerRadius = 4.0
            layer.masksToBounds = true
        } else {
            layer.borderWidth = 0.0
        }
    }
}
