//
//  NavigationController.swift
//  Note
//
//  Created by Sothearith Sreang on 2/12/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    open override func viewWillAppear(_ animated: Bool) {
        initNavigationBar()
    }
    
    private func initNavigationBar() {
        addNavBarShadow()
        initNavBarTintAndColor()
    }
    
    private func addNavBarShadow() {
        self.navigationBar.layer.masksToBounds = false
        self.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationBar.layer.shadowOpacity = 0.5
        self.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationBar.layer.shadowRadius = 2
    }
    
    private func initNavBarTintAndColor() {
        self.navigationBar.tintColor = .white
        self.navigationBar.titleTextAttributes = [
            .foregroundColor : UIColor.white,
            .font: UIFont(name: "Lato-Medium", size: 20)!
        ]
    }
}
