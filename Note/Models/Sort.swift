//
//  Sort.swift
//  Note
//
//  Created by Sothearith Sreang on 2/12/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import Foundation

enum Sort : String {
    case Title = "title"
    case Date = "date"
}
