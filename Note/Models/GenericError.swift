//
//  Error.swift
//  Note
//
//  Created by Sothearith Sreang on 1/12/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import Foundation

import Foundation

enum GenericError : Error {
    case runtimeError(String)
    case nullError(String)
}
