//
//  Line.swift
//  Note
//
//  Created by Sothearith Sreang on 5/12/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import Foundation
import UIKit

struct Line {
    let startPoint: CGPoint
    let endPoint: CGPoint
    let color: UIColor
    let lineWidth: CGFloat
}
