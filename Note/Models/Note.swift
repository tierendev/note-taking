//
//  Note.swift
//  Note
//
//  Created by Sothearith Sreang on 1/12/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import Foundation

struct Note {
    let id: String
    let title: String
    let text: String
    let date: Date?
    let doodle: [Line]?
    
    func dateInString() -> String {
        return date?.toString(format: Date.DDMMMYY_FORMAT) ?? ""
    }
}

extension Note : Equatable {
    static func == (lhs: Note, rhs: Note) -> Bool {
        return lhs.id == rhs.id
    }
}
