//
//  LineMO.swift
//  Note
//
//  Created by Sothearith Sreang on 8/12/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension LineMO {
    
    convenience init(context: NSManagedObjectContext, line: Line) {
        self.init(context: context)
        self.startX = Float(line.startPoint.x)
        self.startY = Float(line.startPoint.y)
        self.endX = Float(line.endPoint.x)
        self.endY = Float(line.endPoint.y)
        self.colorHex = line.color.toHexString()
        self.width = Float(line.lineWidth)
    }
    
    func toLine() -> Line {
        return Line(
            startPoint: CGPoint(x: CGFloat(self.startX), y: CGFloat(self.startY)),
            endPoint: CGPoint(x: CGFloat(self.endX), y: CGFloat(self.endY)),
            color: UIColor(hexString: self.colorHex ?? "#000000"),
            lineWidth: CGFloat(self.width)
        )
    }
    
}
