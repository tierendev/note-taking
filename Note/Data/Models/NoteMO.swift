//
//  NoteMO.swift
//  Note
//
//  Created by Sothearith Sreang on 6/12/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import Foundation
import CoreData

extension NoteMO {
    
    func update(context: NSManagedObjectContext, note: Note) {
        self.id = note.id
        self.title = note.title
        self.text = note.text
        self.date = note.date
        note.doodle?.forEach { line in
            self.addToDoodle(LineMO(context: context, line: line))
        }
    }
 
    func toNote() -> Note {
        
        return Note(
            id: self.id ?? "",
            title: self.title ?? "",
            text: self.text ?? "",
            date: self.date,
            doodle: convertToDoodle(doodle: self.doodle)
        )
    }
    
    private func convertToDoodle(doodle: NSSet?) -> [Line]? {
        if let doodle = doodle {
            var lines: [Line] = []
            for lineMo in doodle {
                if let line = (lineMo as? LineMO)?.toLine() {
                    lines.append(line)
                }
            }
            return lines
        }
        
        return nil
    }
    
}
