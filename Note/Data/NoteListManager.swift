//
//  NoteListManager.swift
//  Note
//
//  Created by Sothearith Sreang on 1/12/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import Foundation
import CoreData
import RxSwift

private let NOTE_ENTITY = "Note"

protocol NoteListManager {
    func getNotes(sort: Sort) -> Single<[Note]>
    func getNote(id: String) -> Single<Note>
    func addNote(note: Note) -> Single<Note>
    func updateNote(note: Note) -> Single<Note>
    func deleteNote(note: Note) -> Completable
}

class NoteListManagerImpl : NoteListManager {
    
    var managedContext: NSManagedObjectContext? = nil
    
    init() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        managedContext = appDelegate?.persistentContainer.viewContext
    }
    
    func getNotes(sort: Sort) -> Single<[Note]> {
        if let managedContext = self.managedContext {
            return Single.deferred {
                let fetchRequest = NSFetchRequest<NoteMO>(entityName: NOTE_ENTITY)
                let sortParam = NSSortDescriptor(key: sort.rawValue, ascending: true)
                fetchRequest.sortDescriptors = [sortParam]
                
                do {
                    let managedObjects = try managedContext.fetch(fetchRequest)
                    let notes = managedObjects.map { object in object.toNote() }
                    return Single.just(notes)
                } catch let error as NSError {
                    return Single.error(GenericError.runtimeError("Could not fetch. \(error), \(error.userInfo)"))
                }
            }
        }
        
        return Single.error(GenericError.runtimeError("managedContext cannot be null"))
    }
    
    func getNote(id: String) -> Single<Note> {
        if let managedContext = self.managedContext {
            return Single.deferred {
                let fetchRequest = NSFetchRequest<NoteMO>(entityName: NOTE_ENTITY)
                fetchRequest.predicate = NSPredicate(format: "id = %@", id)
                
                do {
                    let managedObjects = try managedContext.fetch(fetchRequest)
                    let noteMo = managedObjects[0]
                    return Single.just(noteMo.toNote())
                    
                } catch let error as NSError {
                    return Single.error(GenericError.runtimeError("Could not fetch. \(error), \(error.userInfo)"))
                }
            }
        }
        
        return Single.error(GenericError.runtimeError("managedContext cannot be null"))
    }
    
    func addNote(note: Note) -> Single<Note> {
        if let managedContext = self.managedContext {
            return Single.deferred {
                let entity = NSEntityDescription.entity(forEntityName: NOTE_ENTITY, in: managedContext)!
                let noteMo = NoteMO(entity: entity, insertInto: managedContext)
                noteMo.update(context: managedContext, note: note)
                
                do {
                    try managedContext.save()
                    return Single.just(note)
                } catch let error as NSError {
                    return Single.error(GenericError.runtimeError("Could not save. \(error), \(error.userInfo)"))
                }
            }
        }
        
        return Single.error(GenericError.runtimeError("managedContext cannot be null"))
    }
    
    func updateNote(note: Note) -> Single<Note> {
        if let managedContext = self.managedContext {
            return Single.deferred {
                let fetchRequest = NSFetchRequest<NoteMO>(entityName: NOTE_ENTITY)
                fetchRequest.predicate = NSPredicate(format: "id = %@", note.id)
                
                do {
                    let noteMos = try managedContext.fetch(fetchRequest)
                    let noteMo = noteMos[0]
                    noteMo.update(context: managedContext, note: note)

                    try managedContext.save()
                    return Single.just(note)
                } catch let error as NSError {
                    return Single.error(GenericError.runtimeError("Could not save. \(error), \(error.userInfo)"))
                }
            }
        }
        
        return Single.error(GenericError.runtimeError("managedContext cannot be null"))
    }
    
    func deleteNote(note: Note) -> Completable {
        if let managedContext = self.managedContext {
            return Completable.deferred {
                let fetchRequest = NSFetchRequest<NoteMO>(entityName: NOTE_ENTITY)
                fetchRequest.predicate = NSPredicate(format: "id = %@", note.id)
                
                do {
                    let managedObjects = try managedContext.fetch(fetchRequest)
                    let object = managedObjects[0]
                    managedContext.delete(object)
                    
                    try managedContext.save()
                    return Completable.empty()
                } catch let error as NSError {
                    return Completable.error(GenericError.runtimeError("Could not save. \(error), \(error.userInfo)"))
                }
            }
        }
        
        return Completable.error(GenericError.runtimeError("managedContext cannot be null"))
    }
}
