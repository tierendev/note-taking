//
//  DoodleView.swift
//  Note
//
//  Created by Sothearith Sreang on 5/12/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import UIKit

private let DEFAULT_BRUSH_WIDTH: CGFloat = 15
private let DISTANCE_THRESHOLD: CGFloat = 0
private let BRUSH_COLOR = UIColor.black

class DoodleView: UIView {
    
    @IBInspectable
    var brushWidth: CGFloat = DEFAULT_BRUSH_WIDTH
    
    @IBInspectable
    var isEditable: Bool = true {
        didSet {
            self.isUserInteractionEnabled = isEditable
        }
    }
    
    var isErasing: Bool = false {
        didSet {
            paintColor = isErasing ? (backgroundColor ?? UIColor.white) : BRUSH_COLOR
        }
    }
    
    // states
    var lines: [Line] = [] {
        didSet {
            self.setNeedsDisplay()
        }
    }
    private var lastPoint = CGPoint.zero
    private var paintColor = BRUSH_COLOR
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            lastPoint = touch.location(in: self)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let currentPoint = touch.location(in: self)
            if currentPoint.distance(lastPoint) >= DISTANCE_THRESHOLD {
                addLine(from: lastPoint, to: currentPoint)
                lastPoint = currentPoint
                
                self.setNeedsDisplay()
            }
        }
    }
    
    override func draw(_ rect: CGRect) {
        if let context = UIGraphicsGetCurrentContext() {
            
            context.setLineCap(.round)
            
            for line in lines {
                let relativeStartPoint = toRelativePoint(line.startPoint)
                let realtiveEndPoint = toRelativePoint(line.endPoint)
                let relativeBrushWidth = toRelativeBrushWidth(line.lineWidth)
                context.setStrokeColor(line.color.cgColor)
                context.setLineWidth(relativeBrushWidth)
                context.move(to: relativeStartPoint)
                context.addLine(to: realtiveEndPoint)
                context.strokePath()
            }
        }
    }
    
    private func addLine(from: CGPoint, to: CGPoint) {
        let absoluteCoordFrom = toAbsolutePoint(from)
        let absoluteCoordTo = toAbsolutePoint(to)
        let absoluteBrushWidth = toAbsoluteBrushWidth(brushWidth)
        
        let line = Line(startPoint: absoluteCoordFrom, endPoint: absoluteCoordTo, color: paintColor, lineWidth: absoluteBrushWidth)
        lines.append(line)

    }
    
    private func toRelativePoint(_ absPoint: CGPoint) -> CGPoint {
        let viewSize = self.frame.size
        return CGPoint(x: absPoint.x * viewSize.width, y: absPoint.y * viewSize.height)
    }
    
    private func toAbsolutePoint(_ relativePoint: CGPoint) -> CGPoint {
        let viewSize = self.frame.size
        return CGPoint(x: relativePoint.x / viewSize.width, y: relativePoint.y / viewSize.height)
    }
    
    private func toRelativeBrushWidth(_ absWidth: CGFloat) -> CGFloat {
        let viewSize = self.frame.size
        let area = viewSize.width * viewSize.height
        return absWidth * area
    }
    
    private func toAbsoluteBrushWidth(_ relativeWidth: CGFloat) -> CGFloat {
        let viewSize = self.frame.size
        let area = viewSize.width * viewSize.height
        return relativeWidth / area
    }
}

extension CGPoint {
    func distance(_ from: CGPoint) -> CGFloat {
        return sqrt(pow((self.x - from.x), 2) + pow((self.y - from.y), 2))
    }
}
