//
//  UIView.swift
//  Note
//
//  Created by Sothearith Sreang on 3/12/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    @IBInspectable
    var shadowColorUI: UIColor {
        get {
            if let cgColor = layer.shadowColor {
                return UIColor(cgColor: cgColor)
            } else {
                return UIColor.clear
            }
        }
        set {
            layer.shadowColor = newValue.cgColor
        }
    }
}

