//
//  UITextView.swift
//  Note
//
//  Created by Sothearith Sreang on 4/12/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import Foundation
import UIKit

extension UITextView {
    @IBInspectable
    var maxLines: Int {
        get {
            return textContainer.maximumNumberOfLines
        }
        set {
            textContainer.maximumNumberOfLines = newValue
            textContainer.lineBreakMode = .byTruncatingTail
        }
    }
}
